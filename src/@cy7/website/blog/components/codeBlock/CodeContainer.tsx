import { styled } from "@cy7/design-system";

const CodeContainer = styled("div", {
  backgroundColor: "$obsidian",
  borderRadius: "$2",
  boxShadow: "5px 5px 0 0 $colors$accent",
  marginBottom: "$1",
  overflow: "auto",
});

export default CodeContainer;
