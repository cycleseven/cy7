import { styled } from "@cy7/design-system";

const MdxInlineCode = styled("code", {
  backgroundColor: "$bodyText",
  borderRadius: "$2",
  color: "$bg",
  fontFamily: "$code",
  fontSize: "0.8em",
  padding: "3px 6px",
  whiteSpace: "nowrap",
});

export default MdxInlineCode;
