interface Token {
  types: string[];
}

type SupportedCodeBlockLanguages = "jsx" | "javascript";

export type { Token, SupportedCodeBlockLanguages };
